
<!-- README.md is generated from README.Rmd. Please edit that file -->

# daniel

<!-- badges: start -->

<!-- badgecreatr::minimal_badges(status = "wip", license = "MIT") -->

<!-- badges: end -->

This package aims to simplify common, repetitive tasks I do frequently
in R.

## Installation

You can install this package from GitLab using `devtools`:

``` r
devtools::install_git("https://gitlab.com/danielspracklin/daniel")
```
