
#' Import and set up custom fonts
#'
#' @export
set_up_fonts <- function() {
  if ("Montserrat" %notin% sysfonts::font_families()) {
    sysfonts::font_add_google("Montserrat", "Montserrat")
  }
  showtext::showtext_auto()
}
