
#' Use a standardized ggplot2 theme
#'
#' @param base_size (`numeric`) Overall font size
#' @param font_family (`character`) Font family for titles, labels and legends.
#' Defaults to Montserrat.
#' @importFrom ggplot2 element_line
#' @importFrom ggplot2 element_blank
#' @importFrom ggplot2 element_text
#' @importFrom grid unit
#'
#' @export
theme_daniel <- function(base_size = 14, font_family = "Montserrat") {

  checkmate::assert_numeric(base_size, lower = 1, upper = 100)
  checkmate::assert_character(font_family)

  tiny_size  <- base_size * 11 / 14
  small_size <- base_size * 12 / 14
  large_size <- base_size * 16 / 14

  set_up_fonts()

  ggplot2::theme_minimal(base_size = base_size,
                         base_family = font_family) +
    ggplot2::theme(panel.grid = element_line(size = 0.5, colour = "grey85"),
                   panel.grid.minor = element_blank(),
                   panel.spacing = unit(2, "lines"),
                   panel.background = element_blank(),
                   strip.background = element_blank(),
                   strip.placement = "outside",
                   axis.ticks = element_blank(),
                   legend.position = "bottom",
                   legend.text = element_text(size = small_size),
                   legend.background = element_blank(),
                   legend.key = element_blank(),
                   plot.title = element_text(size = large_size, face = "bold",
                                             hjust = 0),
                   plot.subtitle = element_text(size = small_size,
                                                hjust = 0),
                   axis.title = element_text(size = base_size),
                   axis.text = element_text(size = small_size),
                   strip.text = element_text(size = base_size),
                   plot.caption = element_text(size = tiny_size,
                                               hjust = 1),
                   complete = T)
}
